

function problem1(data, ids, callbacks) {
    try {              
        let employee = data.employees        
        let problem1Ofresult = employee.filter(Element => {
            if (ids.includes(Element.id)) {
                return Element
            }
        })
        callbacks(null, problem1Ofresult)
    } catch (err) {

        callbacks(err, null)
    }
}



function problem2(data, callbacks) {
    try {
       
        let employee = data.employees
        let problem2Result = employee.reduce((acc, cv) => {
            if (acc[cv.company]) {
                acc[cv.company].push(cv)
            } else {
                acc[cv.company] = []
                acc[cv.company].push(cv)
            }
            return acc
        }, {})
        callbacks(null, problem2Result)
    } catch (err) {
        callbacks(err, null)
    }
}

function problem3(data, callbacks) {
    try {
        
        let employee = data.employees
        let problem3OfResult = employee.reduce((acc, cv) => {
            if (cv.company === "Powerpuff Brigade") {
                if (acc[cv.company]) {
                    acc[cv.company].push(cv)
                } else {
                    acc[cv.company] = []
                    acc[cv.company].push(cv)
                }
            }
            return acc
        }, {})
        callbacks(null, problem3OfResult)
    } catch (err) {
        callbacks(err, null)
    }
}


function problem4(data, callbacks) {
    try {
        
        let employee = data.employees
        let problem4OfResult = employee.filter((Element) => {
            if (Element.id !== 2) {
                return Element
            }

        })
        callbacks(null, problem4OfResult)
    } catch (err) {
        callbacks(err, null)
    }
}

function problem5(data, callbacks) {
    try {
        let employee = data.employees

        let problem5OfResult = employee.sort((Element1, Element2) => {
            if (Element1.company < Element2.company) {
                return -1
            } else {
                return 1
            }

            if (Element1.id < Element2.id) {
                return -1
            } else {
                return 1
            }
            return 0
        })
        callbacks(null, problem5OfResult)
    } catch (err) {
        callbacks(err, null)
    }
}

function problem6(data, callbacks) {
    try {
        let employee = data.employees
        let ind93 = employee.find(Element => Element.id === 93)
        let indx92 = employee.find(Element => Element.id === 92)
        let temp = ind93.company
        ind93.company = indx92.company;
        indx92.company = temp;
        callbacks(null, employee)
    } catch (err) {
        callbacks(err, null)
    }
}

function problem7(data,callbacks) {
    try{
        
        let employee = data.employees
        let resultOfproblem7 = employee.filter((Element)=>{
            if(Element.id %2 ===0){
               return Element.Birthday = Date.now()
            }
        })
        
        callbacks(null,resultOfproblem7)

    }catch(err){
        callbacks(err,null)
    }
}

module.exports = {problem1,problem2,problem3,problem4,problem5,problem6,problem7}

